# TODO: License here

from app import db

class Tag(db.Model):
    __tablename__ = 'tags'
    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.Text, unique=True)

    @classmethod
    def from_json(cls, json_data):
        tag = cls()
        tag.name = json_data['name']

        return tag

    def to_json(self):
        json_data = {
            'name': self.name
        }

        return json_data

    def __repr__(self):
        return f'<Tag "{self.name}">'


# Association tables

tags_and_characters = db.Table(
    'tags_and_characters',
    db.Model.metadata,
    db.Column('tags_id', db.Integer, db.ForeignKey('tags.id')),
    db.Column('characters_id', db.Integer, db.ForeignKey('characters.id'))
)

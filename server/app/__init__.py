# -*- coding: utf-8 -*-

#TODO: License blurb here

# Urpchat Server

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

db = SQLAlchemy()

def create_app(config_object):
    app = Flask(__name__)
    app.config.from_object(config_object)
    migrate = Migrate(app, db)
    db.init_app(app)

    from .tags import tags_blueprint
    app.register_blueprint(tags_blueprint, url_prefix='/tags')
    from .tags.models import Tag

    from .characters import characters_blueprint
    app.register_blueprint(characters_blueprint, url_prefix='/characters')
    from .characters.models import Character

    return app

# TODO: License here

from app import db

from ..tags.models import Tag, tags_and_characters

class Character(db.Model):
    """A character's profile information"""
    __tablename__='characters'
    id=db.Column(db.Integer, primary_key=True)

    name=db.Column(db.Text, unique=True)
    gender=db.Column(db.Text)
    description=db.Column(db.Text)
    tags=db.relationship(
        'Tag',
        secondary=tags_and_characters,
        backref='characters'
    )

    @classmethod
    def from_json(cls, json_data):
        character = cls()
        character.name=json_data['name']
        character.gender=json_data['gender']
        character.description=json_data['description']

        return character

    def to_json(self):
        json_data = {
            'name': self.name,
            'gender': self.gender,
            'description': self.description
        }
        
        return json_data

    def __repr__(self):
        return f'<Character "{self.name}">'

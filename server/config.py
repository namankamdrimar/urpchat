import os

class Config():
    DEBUG=False
    TESTING=False
    SECRET_KEY=os.environ.get('URPCHAT_SECRET_KEY')


class Development(Config):
    DEBUG=True
    SQLALCHEMY_DATABASE_URI=os.environ.get('URPCHAT_DB_DEVELOPMENT_URI')


class Testing(Config):
    TESTING=True
    SQLALCHEMY_DATABASE_URI=os.environ.get('URPCHAT_DB_TESTING_URI')


class Production(Config):
    SQLALCHEMY_DATABASE_URI=os.environ.get('URPCHAT_DB_PRODUCTION_URI')
